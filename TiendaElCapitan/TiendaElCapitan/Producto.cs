﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TiendaElCapitan
{
    class Producto
    {
        private int Id;
        private int Cantidad;
        private double ValorUnitario;
        private string Nombre;

        public Producto(int Cantidad, double ValorUnitario, string Nombre) {
            this.Cantidad = Cantidad;
            this.ValorUnitario = ValorUnitario;
            this.Nombre = Nombre;
        }

        public int getCantidad() {
            return Cantidad;
        }

        public void setCantidad(int Cantidad) {
            this.Cantidad = Cantidad;
        }
        public double getValorUnitario() {
            return ValorUnitario;
        }

        public void setValorUnitario(double ValorUnitario) {
            this.ValorUnitario = ValorUnitario;
        }

        public string getNombre() {
            return Nombre;
        }

        public void setNombre(string Nombre) {
            this.Nombre = Nombre;
        }
    }
}
