﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TiendaElCapitan
{
    class Proveedor : Persona
    {
        private int Id;
        private string ClaseDeProductos;
        private string Horario;

        public Proveedor(string ClaseDeProductos, string Horario, string Nombre, string Apellido, string CI, string Telefono) : base(Nombre, Apellido, CI, Telefono){
            this.ClaseDeProductos = ClaseDeProductos;
            this.Horario = Horario;
        }

        public string getClaseDeProductos() {
            return ClaseDeProductos;
        }
        public void setClaseDeProductos(string ClaseDeProductos) {
            this.ClaseDeProductos = ClaseDeProductos;
        }

        public string getHorario()
        {
            return Horario;
        }
        public void setHorario(string Horario)
        {
            this.Horario = Horario;
        }

    }
}
