﻿using System;
using System.Collections.Generic;

namespace TiendaElCapitan
{
    class Program
    {
        static void Main(string[] args)
        {
            Producto gaseosa = new Producto(1, 1, "CocaCola 1 lt");
            Producto atun = new Producto(1, 1.25, "Isabel 250gr");
            Producto chifle = new Producto(2, 0.75, "ChiflesMario 200gr");
            Producto pimienta = new Producto(1, 0.30, "PimientaSL 50gr");

            

            List<Producto> ListaProductos = new List<Producto>();
            ListaProductos.Add(gaseosa);
            ListaProductos.Add(atun);
            ListaProductos.Add(chifle);
            ListaProductos.Add(pimienta);

            Cuenta cuenta1 = new Cuenta(0, ListaProductos, "Luis", "Alcantara", "1313131395", "0999999999");
            cuenta1.setSaldo(cuenta1.CalcularSaldo(ListaProductos));

            Console.WriteLine("Cuenta :"+cuenta1.getNombre()+" "+cuenta1.getApellido() + "\n"+
                               "Detalle: \n"+ cuenta1.DetallarProductos()
                               +"Saldo: " + cuenta1.getSaldo() + " $\n");


            Proveedor proveedorAgua = new Proveedor("Agua Embotellada", "Martes, Viernes", "Aloe", "Vera", "1234567895", "0420420420");
            Console.WriteLine("Proveedor:\nNombre: " + proveedorAgua.getNombre() + " " + proveedorAgua.getApellido()
                                + "\nCedula: " + proveedorAgua.getCI() + "    Telefono: " + proveedorAgua.getTelefono()
                                + "\nProducto: " + proveedorAgua.getClaseDeProductos()
                                + "\nHorario :" + proveedorAgua.getHorario()) ;
        }
    }
}
