﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TiendaElCapitan
{
    class Persona
    {
        private int Id;
        private string Nombre;
        private string Apellido;
        private string CI;
        private string Telefono;

        public Persona(string Nombre, string Apellido, string CI, string Telefono)
        {
            this.Nombre = Nombre;
            this.Apellido = Apellido;
            this.CI = CI;
            this.Telefono = Telefono;
        }

        public int getId()
        {
            return Id;
        }

        public void setId(int Id)
        {
            this.Id = Id;
        }
        public string getNombre() {
            return Nombre;
        }

        public void setNombre(string Nombre) {
            this.Nombre = Nombre;
        }
        public string getApellido()
        {
            return Apellido;
        }

        public void setApellido(string Apellido)
        {
            this.Apellido = Apellido;
        }
        public string getCI()
        {
            return CI;
        }

        public void setCI(string CI)
        {
            this.CI = CI;
        }

        public string getTelefono() {
            return Telefono;
        }

        public void setTelefono(string Telefono) {
            this.Telefono = Telefono;
        }

    }
}
