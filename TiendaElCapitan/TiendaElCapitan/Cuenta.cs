﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TiendaElCapitan
{
    class Cuenta : Persona
    {
        private int Id;
        private double Saldo;
        private List<Producto> Productos;

        public Cuenta(double Saldo, List<Producto> Productos, string Nombre, string Apellido, string CI, string Telefono) : base (Nombre, Apellido, CI, Telefono){
            this.Saldo = Saldo;
            this.Productos = Productos;
        }

        public double getSaldo() {
            return Saldo;
        }

        public void setSaldo(double Saldo) {
            this.Saldo = Saldo;
        }

        public List<Producto> getProductos()
        {
            return Productos;
        }

        public void setProductos(List<Producto> Productos)
        {
            int y = this.Productos.Count;
            for (int i = 0; i < Productos.Count; i++) {
                this.Productos[y] = Productos[i];
                y++;
            }
        }

        public void AumentarSaldo(double Saldo) {
            this.Saldo = this.Saldo + Saldo;
        }

        public double CalcularSaldo(List<Producto> Productos) {
            double Saldo = 0;
            for (int i = 0; i < Productos.Count; i++) {
                Saldo += Productos[i].getValorUnitario() * Productos[i].getCantidad();
            }
            return Saldo;
        }

        public void Pago(double Valor)
        {
            this.Saldo = this.Saldo - Valor;
        }

        public string DetallarProductos() {
            string txt = "";
            for (int i = 0; i < Productos.Count; i++) {
                txt += Productos[i].getCantidad() + " | " + Productos[i].getNombre() + "\t| " + Productos[i].getValorUnitario()+" $\n";
            }
            return txt;
        }
    }


}
